package com.notyteam.bhabreathwork

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notyteam.bhabreathwork.base.mvvm.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(app: Application) : BaseViewModel(app) {
    private val authLivaData = MutableLiveData<Any>()

    fun auth(password: String, userName: String): LiveData<Any> {
        api.authorize(
            password, userName, onSuccess = {
                authLivaData.postValue(it)
            },
            onError = { errorMessage, _ ->
                when (errorMessage) {
                    "username does not exist" -> authLivaData.postValue(
                        app.applicationContext.getString(
                            R.string.username_does_not_exist
                        )
                    )
                    "password does not match" -> authLivaData.postValue(
                        app.applicationContext.getString(
                            R.string.password_does_not_match
                        )
                    )
                    "account status is: Locked" -> authLivaData.postValue(
                        app.applicationContext.getString(R.string.account_status_is_locked)
                    )
                    else -> authLivaData.postValue(errorMessage)
                }
            }
        ).call()

        return authLivaData
    }

    private var registrationLiveData = MutableLiveData<Any>()

    fun registrationUser(
        firstName: String,
        lastName: String,
        gender: String,
        birthday: String?,
        email: String,
        userName: String,
        password: String
    ): LiveData<Any> {
        api.registerUser(
            firstName,
            lastName,
            gender,
            birthday,
            email,
            userName,
            password,
            onSuccess = {
                registrationLiveData.postValue(it)
            },
            onError = { errorMessage, _ ->
                Log.d("LOG_D", "MainViewModel.kt : registrationUser ->  ");
                when (errorMessage) {
                    "email already taken" -> registrationLiveData.postValue(
                        app.applicationContext.getString(
                            R.string.email_already_taken
                        )
                    )
                    "username already taken”" -> registrationLiveData.postValue(
                        app.applicationContext.getString(
                            R.string.username_already_taken
                        )
                    )
                    "invalidate client info”" -> registrationLiveData.postValue(
                        app.applicationContext.getString(
                            R.string.invalidate_client_info
                        )
                    )
                    "invalidate invite token”" -> registrationLiveData.postValue(
                        app.applicationContext.getString(
                            R.string.invalidate_invite_token
                        )
                    )
                    else -> registrationLiveData.postValue(errorMessage)
                }
            }).call()

        return registrationLiveData
    }

    override fun onClear() {
    }
}