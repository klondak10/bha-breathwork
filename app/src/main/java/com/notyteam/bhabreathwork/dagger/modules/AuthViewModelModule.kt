package com.notyteam.bhabreathwork.dagger.modules

import androidx.lifecycle.ViewModel
import com.notyteam.bhabreathwork.MainViewModel
import com.notyteam.bhabreathwork.dagger.ViewModelKey
import com.notyteam.bhabreathwork.dagger.scopes.FragmentScope
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AuthViewModelModule {
    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindAuthActivityViewModel(model: MainViewModel): ViewModel
}