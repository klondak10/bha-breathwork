package com.notyteam.bhabreathwork.dagger.components

import android.app.Application
import com.notyteam.bhabreathwork.api.Api
import com.notyteam.bhabreathwork.api.CallbackWrapper
import com.notyteam.bhabreathwork.base.mvvm.BaseViewModel
import com.notyteam.bhabreathwork.dagger.modules.ApiModule
import com.notyteam.bhabreathwork.dagger.modules.AppModule
import com.notyteam.bhabreathwork.dagger.modules.FragmentModule
import com.notyteam.bhabreathwork.dagger.modules.ViewModelFactoryModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class,
        ApiModule::class,
        ViewModelFactoryModule::class]
)
interface AppComponent {
    fun plus(fragmentModule: FragmentModule): FragmentComponent

    fun inject(app: Application)
    fun inject(target: CallbackWrapper<Any, Any>)
    fun inject(target: Api)
    fun inject(target: BaseViewModel)
}