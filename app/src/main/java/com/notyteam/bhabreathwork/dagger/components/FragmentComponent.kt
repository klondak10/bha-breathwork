package com.notyteam.bhabreathwork.dagger.components

import com.notyteam.bhabreathwork.base.BaseActivity
import com.notyteam.bhabreathwork.base.BaseFragment
import com.notyteam.bhabreathwork.base.mvvm.BaseViewModel
import com.notyteam.bhabreathwork.dagger.modules.FragmentModule
import com.notyteam.bhabreathwork.dagger.modules.AuthViewModelModule
import com.notyteam.bhabreathwork.dagger.scopes.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [FragmentModule::class,
        AuthViewModelModule::class]
)
interface FragmentComponent {
    fun inject(target: BaseActivity<BaseViewModel>)
    fun inject(target: BaseFragment<BaseViewModel>)
}