package com.notyteam.bhabreathwork.dagger.modules

import androidx.lifecycle.ViewModelProvider
import com.notyteam.bhabreathwork.base.mvvm.DaggerViewModelFactory
import com.notyteam.bhabreathwork.dagger.scopes.FragmentScope
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    @FragmentScope
    abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory
}