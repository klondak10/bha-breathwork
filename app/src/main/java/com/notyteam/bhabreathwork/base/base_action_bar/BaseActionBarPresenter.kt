package com.notyteam.bhabreathwork.base.base_action_bar

import io.reactivex.disposables.CompositeDisposable

class BaseActionBarPresenter(var actionBarView: ActionBarContract.View) : ActionBarContract.Presenter {
    private val disposable = CompositeDisposable()

    override fun setupView() {
        TODO("Not yet implemented")
    }

    override fun setupActions() {
        actionBarView.showActionBar(false)
        disposable.add(actionBarView.leftButtonAction().subscribe { leftButtonAction() })
    }

    override fun leftButtonAction() {
        TODO("Not yet implemented")
    }

    override fun rightButtonAction() {
        TODO("Not yet implemented")
    }

    override fun dispose() {
        disposable.clear()
    }

    override fun start() {
        setupView()
        setupActions()
    }

    override fun stop() {
        dispose()
    }
}