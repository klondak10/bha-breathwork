package com.notyteam.bhabreathwork.base.base_action_bar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.jakewharton.rxbinding2.view.RxView
import com.notyteam.bhabreathwork.R
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.toolbar.*

class BaseActionBarView(private var root: View?,
                        override val containerView: View?) :
    LayoutContainer, ActionBarContract.View {

    override fun showActionBar(show: Boolean) {
        root?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showLeftButton(show: Boolean) {
        flLeftContainer.visibility = if (show) View.VISIBLE else View.INVISIBLE
        flLeftContainer.isEnabled = show

    }

    override fun setupLeftButton(view: View) {
        addViewSafe(flLeftContainer, view)
    }

    override fun showRightButton(show: Boolean) {
        flRightContainer.visibility = if (show) View.VISIBLE else View.INVISIBLE
        flRightContainer.isEnabled = show
    }


    override fun getRightContainer(): ViewGroup = flRightContainer
    override fun setDrawerMenu(inflater: LayoutInflater): View {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun setupRightButton(view: View) {
        addViewSafe(flRightContainer, view)
    }

    override fun showCenterText(show: Boolean) {
        tvCenterText.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun setupCenterText(res: Int) {
        tvCenterText.setText(res)
    }

    override fun setupCenterText(string: String) {
        tvCenterText.text = string
    }

    override fun leftButtonAction(): Observable<Any> {
        return RxView.clicks(flLeftContainer)
    }

    override fun getLeftButton(): View = flLeftContainer

    override fun rightButtonAction(): Observable<Any> {
        return RxView.clicks(flRightContainer)
    }

    private fun addViewSafe(@Nullable parentNew: ViewGroup?, @Nullable view: View?) {
        parentNew?.let {
            it.removeAllViews()
            parentNew.addView(view)
        }
    }

    override fun resetView(boolean: Boolean) {
        if (boolean) {
            flLeftContainer.removeView(flLeftContainer.rootView)
        }
    }

    override fun setBackButton(inflater: LayoutInflater): View {
        setupLeftButton(inflater.inflate(R.layout.ab_back, null))
        return getLeftButton()
    }
}