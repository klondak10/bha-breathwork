package com.notyteam.bhabreathwork.api.responses

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @field:SerializedName("login")
    val login: Login?
) {
    data class Login(
        @field:SerializedName("expiredAt")
        val expiredAt: Int?,
        @field:SerializedName("id")
        val id: String?
    )
}