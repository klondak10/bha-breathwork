package com.notyteam.bhabreathwork.api

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class BaseResponse(
    @field:SerializedName("error_message")
    val errorMessage: String = "",

    @field:SerializedName("status_code")
    val statusCode: String = ""
)