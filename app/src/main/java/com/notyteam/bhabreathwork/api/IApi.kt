package com.notyteam.bhabreathwork.api

import com.notyteam.bhabreathwork.api.requests.AuthorizeRequest
import com.notyteam.bhabreathwork.api.requests.RegistrationRequest
import com.notyteam.bhabreathwork.api.responses.FullAuthResponse
import com.notyteam.bhabreathwork.api.responses.RegistrationResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface IApi {
    @Headers("Content-Type: application/json")
    @POST("/query")
    fun authorize(@Body authorizeRequest: AuthorizeRequest): Single<ApiResponse<FullAuthResponse>>

    @Headers("Content-Type: application/json")
    @POST("/query")
    fun registerUser(@Body authorizeRequest: RegistrationRequest): Single<ApiResponse<RegistrationResponse>>
}