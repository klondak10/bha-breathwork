package com.notyteam.bhabreathwork.api

import com.google.gson.annotations.SerializedName

class ApiResponse<T> {
    @field:SerializedName("errors")
    val error: List<ApiError>? = null

    @field:SerializedName("data")
    val date: T? = null
}