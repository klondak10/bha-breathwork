package com.notyteam.bhabreathwork.api

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class DefaultResponse(

    @field:SerializedName("values")
    var values: ArrayList<List<String>>
)