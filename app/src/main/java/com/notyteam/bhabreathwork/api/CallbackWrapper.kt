package com.notyteam.bhabreathwork.api

import android.util.Log
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

class CallbackWrapper<T, V>(
    private val retrofit: Retrofit,
    private var onSuccessResponse: (response: V) -> Unit,
    private var onError: (errorMessage: String, errorCode: Int) -> Unit
) : DisposableSingleObserver<T>() {

    companion object {
        const val ERROR_WRONG_SMS_CODE = 700
        const val ERROR_UNEXPECTED_CODE = 666
        const val ERROR_CONVERTING_DATA_CODE = 601
        const val ERROR_INTERNET_CONNECTION_CODE = 600
        const val ERROR_NOT_FOUND_CODE = 404
        const val ERROR_UNAUTHORIZED = 401
    }

    var request: Single<T>? = null
    //private var repeatingUnauthorizedRequestCount = 0

    override fun onSuccess(response: T) {
        val resp = response as ApiResponse<V>
        if (resp.error != null) {
            onError(resp.error[0].message!!, 200)
        }

        if (resp.date != null) {
            onSuccessResponse(resp.date)
        }
    }

    override fun onError(throwable: Throwable) {
        val errorMessage: String
        val errorCode: Int
        if (throwable is IOException) {
            errorMessage = "Проверьте интернет соединение"
            errorCode = ERROR_INTERNET_CONNECTION_CODE
        } else if (throwable is HttpException) {
            Log.d("D_LOG", "CallbackWrapper.onError() -> (throwable is HttpException)")
            val errorWrapper = ErrorWrapper()
            val error = errorWrapper.parseError(retrofit, throwable.response())

            if (error != null) {
                errorMessage = error.errorMessage
                errorCode = throwable.code()
            } else {
                errorMessage =
                        /*contextWeakReference?.get()?.getString(R.string.error_unexpected)?:*/
                    "Непредвиденная ошибка"
                errorCode = ERROR_UNEXPECTED_CODE
            }

        } else if (throwable is IllegalStateException) {
            errorMessage = "Ошибка преобразования данных"
            errorCode = ERROR_CONVERTING_DATA_CODE
        } else {

            if (throwable.message!!.contains("HTTP 401 Unauthorized")) {
                errorMessage = "Your request was made with invalid credentials."
                errorCode = ERROR_UNEXPECTED_CODE
            } else {
                errorMessage = throwable.message.toString()
                errorCode = ERROR_UNEXPECTED_CODE
            }
        }
        onError(errorMessage, errorCode)
    }

    /* private fun onUnauthorized() {
         val refreshToken = PaperIO.getRefreshToken()
         loginApi.refreshToken(RefreshTokenRequest(refreshToken)) { res ->
             when (res) {
                 is Success -> {
                     if (res.value.accessToken?.isNotEmpty() == true) {
                         PaperIO.setAccessToken(res.value.accessToken)

                         repeatingUnauthorizedRequestCount++
                         if (repeatingUnauthorizedRequestCount > 2) {
                             repeatingUnauthorizedRequestCount = 0
                             onResult(Failure(t.message(), t.code()))
                             contextWeakReference?.get()?.let {
                                 val intent = Intent(it, LoginActivity::class.java)
                                 intent.flags =
                                     Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                 val pi = PendingIntent.getActivity(it, 0, intent, 0)
                                 pi.send()
                             }
                         } else {
                             val callback = CallbackWrapper<T, V>(contextWeakReference, onResult)
                             callback.repeatingUnauthorizedRequestCount =
                                 repeatingUnauthorizedRequestCount
                             callback.request = request
                             request?.doOnSuccess {
                                 callback.onSuccess(it)
                             }?.doOnError {
                                 callback.onError(it)
                             }?.subscribe({}, {})
                             request?.subscribeWith(callback)
                         }
                     }
                 }
                 is Failure -> {
                     onResult(Failure(res.errorMessage, res.errorCode))
                 }
             }
         }.subscribe({}, {})
     }*/

    private class ErrorWrapper {
        fun parseError(retrofit: Retrofit, response: Response<*>?): BaseResponse? {
            val converter: Converter<ResponseBody, BaseResponse> = retrofit
                .responseBodyConverter(BaseResponse::class.java, emptyArray())
            try {
                response?.errorBody()?.let {
                    return converter.convert(it)
                }
            } catch (e: Exception) {
                return BaseResponse()
            }
            return BaseResponse()
        }
    }
}