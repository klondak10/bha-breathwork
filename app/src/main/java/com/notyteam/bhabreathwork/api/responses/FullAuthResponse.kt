package com.notyteam.bhabreathwork.api.responses

import com.google.gson.annotations.SerializedName


data class FullAuthResponse(
    @field:SerializedName("login")
    val login: Login?
) {
    data class Login(
        @field:SerializedName("expiredAt")
        val expiredAt: Int?,
        @field:SerializedName("id")
        val id: String?,
        @field:SerializedName("user")
        val user: User?
    ) {
        data class User(
            @field:SerializedName("address")
            val address: String?,
            @field:SerializedName("caregiver")
            val caregiver: Any?,
            @field:SerializedName("client")
            val client: Client?,
            @field:SerializedName("createdAt")
            val createdAt: String?,
            @field:SerializedName("email")
            val email: String?,
            @field:SerializedName("enabledProcedures")
            val enabledProcedures: List<EnabledProcedure?>?,
            @field:SerializedName("firstName")
            val firstName: String?,
            @field:SerializedName("id")
            val id: String?,
            @field:SerializedName("isPasswordExpired")
            val isPasswordExpired: Boolean?,
            @field:SerializedName("language")
            val language: String?,
            @field:SerializedName("lastAccessed")
            val lastAccessed: String?,
            @field:SerializedName("lastName")
            val lastName: String?,
            @field:SerializedName("lastResetPassword")
            val lastResetPassword: Any?,
            @field:SerializedName("measuringSystem")
            val measuringSystem: String?,
            @field:SerializedName("namePrefix")
            val namePrefix: String?,
            @field:SerializedName("nameSuffix")
            val nameSuffix: String?,
            @field:SerializedName("phone")
            val phone: String?,
            @field:SerializedName("roles")
            val roles: List<String?>?,
            @field:SerializedName("status")
            val status: String?,
            @field:SerializedName("userType")
            val userType: String?,
            @field:SerializedName("username")
            val username: String?
        ) {
            data class Client(
                @field:SerializedName("audioFile")
                val audioFile: String?,
                @field:SerializedName("audioGuide")
                val audioGuide: Boolean?,
                @field:SerializedName("audioType")
                val audioType: String?,
                @field:SerializedName("caregiverRegistrationNumber")
                val caregiverRegistrationNumber: String?,
                @field:SerializedName("dateOfBirth")
                val dateOfBirth: String?,
                @field:SerializedName("difficulty")
                val difficulty: String?,
                @field:SerializedName("disclaimerAccepted")
                val disclaimerAccepted: Boolean?,
                @field:SerializedName("gender")
                val gender: String?,
                @field:SerializedName("guide")
                val guide: Guide?,
                @field:SerializedName("heathNotes")
                val heathNotes: String?,
                @field:SerializedName("id")
                val id: String?,
                @field:SerializedName("lastTested")
                val lastTested: String?,
                @field:SerializedName("preferredBreathRate")
                val preferredBreathRate: String?
            ) {
                data class Guide(
                    @field:SerializedName("caregiver")
                    val caregiver: Any?,
                    @field:SerializedName("createdAt")
                    val createdAt: String?,
                    @field:SerializedName("exhaleTime")
                    val exhaleTime: Int?,
                    @field:SerializedName("holdTime")
                    val holdTime: Int?,
                    @field:SerializedName("id")
                    val id: String?,
                    @field:SerializedName("inhaleTime")
                    val inhaleTime: Int?,
                    @field:SerializedName("isPredefined")
                    val isPredefined: Boolean?,
                    @field:SerializedName("name")
                    val name: String?,
                    @field:SerializedName("pauseTime")
                    val pauseTime: Int?
                )
            }

            data class EnabledProcedure(
                @field:SerializedName("createdAt")
                val createdAt: String?,
                @field:SerializedName("id")
                val id: String?,
                @field:SerializedName("isEnabled")
                val isEnabled: Boolean?,
                @field:SerializedName("name")
                val name: String?,
                @field:SerializedName("procedureId")
                val procedureId: String?
            )
        }
    }
}