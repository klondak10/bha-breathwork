package com.notyteam.bhabreathwork.api.requests

import com.google.gson.annotations.SerializedName

data class RegistrationRequest(
    @field: SerializedName("operationName")
    val operationName: Any?,
    @field: SerializedName("query")
    val query: String?,
    @field: SerializedName("variables")
    val variables: Variables?
) {
    data class Variables(
        @field: SerializedName("input")
        val input: Input?
    )

    data class Input(
        @field: SerializedName("account")
        val account: Account?,
        @field: SerializedName("caregiver")
        val caregiver: Any?,
        @field: SerializedName("client")
        val client: Client?
    ) {
        data class Account(
            @field: SerializedName("address")
            val address: String?,
            @field: SerializedName("email")
            val email: String?,
            @field: SerializedName("firstName")
            val firstName: String?,
            @field: SerializedName("language")
            val language: String?,
            @field: SerializedName("lastName")
            val lastName: String?,
            @field: SerializedName("measuringSystem")
            val measuringSystem: String?,
            @field: SerializedName("namePrefix")
            val namePrefix: String?,
            @field: SerializedName("nameSuffix")
            val nameSuffix: String?,
            @field: SerializedName("password")
            val password: String?,
            @field: SerializedName("phone")
            val phone: String?,
            @field: SerializedName("userType")
            val userType: String?,
            @field: SerializedName("username")
            val username: String?
        )

        data class Client(
            @field: SerializedName("acceptClient")
            val acceptClient: Any?,
            @field: SerializedName("audioFile")
            val audioFile: String?,
            @field: SerializedName("audioGuide")
            val audioGuide: Boolean?,
            @field: SerializedName("audioType")
            val audioType: String?,
            @field: SerializedName("caregiverInviteToken")
            val caregiverInviteToken: Any?,
            @field: SerializedName("caregiverRegistrationNumber")
            val caregiverRegistrationNumber: String?,
            @field: SerializedName("dateOfBirth")
            val dateOfBirth: String?,
            @field: SerializedName("difficulty")
            val difficulty: String?,
            @field: SerializedName("disclaimerAccepted")
            val disclaimerAccepted: Boolean?,
            @field: SerializedName("gender")
            val gender: String?,
            @field: SerializedName("guideId")
            val guideId: Long?,
            @field: SerializedName("healthNotes")
            val healthNotes: String?,
            @field: SerializedName("preferredBreathRate")
            val preferredBreathRate: String?
        )
    }
}