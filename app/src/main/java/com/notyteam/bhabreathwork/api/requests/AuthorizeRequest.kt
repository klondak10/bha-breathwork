package com.notyteam.bhabreathwork.api.requests

data class AuthorizeRequest(
    val query: String,
    val variables: Variables
) {
    val operationName: String = ""

    data class Variables(
        val input: Input
    )

    data class Input(
        val password: String,
        val username: String
    )
}