package com.notyteam.bhabreathwork.api

import android.content.Context
import com.notyteam.bhabreathwork.App
import com.notyteam.bhabreathwork.api.requests.AuthorizeRequest
import com.notyteam.bhabreathwork.api.requests.RegistrationRequest
import com.notyteam.bhabreathwork.api.responses.FullAuthResponse
import com.notyteam.bhabreathwork.api.responses.RegistrationResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class Api @Inject constructor(
    val context: Context,
    val retrofit: Retrofit
) {
    @Inject
    lateinit var api: IApi

    companion object {
        const val authQuery: String =
            "mutation login(\$input:Login!) { login(input: \$input){id\texpiredAt\t}}"
        const val authQueryFull: String =
            "mutation login(\$input:Login!) { login(input: \$input){id\tuser {id\tuserType\tfirstName\tlastName\tnamePrefix\tnameSuffix\tphone\temail\tusername\troles\taddress\tstatus\tlanguage\tmeasuringSystem\tlastAccessed\tlastResetPassword\tisPasswordExpired\tcreatedAt\tcaregiver {id\tbusinessName\tshareCode\t}client {id\tcaregiverRegistrationNumber\tgender\tdateOfBirth\theathNotes\tdifficulty\tguide {id\tcaregiver {id\tbusinessName\tshareCode\t}name\tinhaleTime\tholdTime\texhaleTime\tpauseTime\tisPredefined\tcreatedAt\t}preferredBreathRate\taudioGuide\taudioType\taudioFile\tdisclaimerAccepted\tlastTested\t}enabledProcedures {id\tprocedureId\tname\tisEnabled\tcreatedAt\t}}expiredAt\t}}"
        const val registrationQuery: String =
            "mutation register(\$input:NewUser!) { register(input: \$input){id\tuserType\tfirstName\tlastName\tnamePrefix\tnameSuffix\tphone\temail\tusername\troles\taddress\tstatus\tlanguage\tmeasuringSystem\tlastAccessed\tlastResetPassword\tisPasswordExpired\tcreatedAt\tcaregiver {id\tbusinessName\tshareCode\t}client {id\tcaregiverRegistrationNumber\tgender\tdateOfBirth\theathNotes\tdifficulty\tguide {id\tcaregiver {id\tbusinessName\tshareCode\t}name\tinhaleTime\tholdTime\texhaleTime\tpauseTime\tisPredefined\tcreatedAt\t}preferredBreathRate\taudioGuide\taudioType\taudioFile\tdisclaimerAccepted\tlastTested\t}enabledProcedures {id\tprocedureId\tname\tisEnabled\tcreatedAt\t}}}"
    }

    init {
        App[context].appComponent.inject(this)
    }

    fun authorize(
        password: String,
        userName: String,
        onSuccess: (response: FullAuthResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<ApiResponse<FullAuthResponse>> {
        return initRequestSingleForResult(
            api.authorize(
                AuthorizeRequest(
                    authQueryFull,
                    AuthorizeRequest.Variables(AuthorizeRequest.Input(password, userName))
                )
            ),
            onSuccess,
            onError
        )
    }

    fun registerUser(
        firstName: String,
        lastName: String,
        gender: String,
        birthday: String?,
        email: String,
        userName: String,
        password: String,
        onSuccess: (response: RegistrationResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<ApiResponse<RegistrationResponse>> {
        return initRequestSingleForResult(
            api.registerUser(
                RegistrationRequest(
                    null,
                    registrationQuery,
                    RegistrationRequest.Variables(
                        RegistrationRequest.Input(
                            RegistrationRequest.Input.Account(
                                "",
                                email,
                                firstName,
                                "en-US",
                                lastName,
                                "Standard",
                                "",
                                "",
                                password,
                                "",
                                "Client",
                                userName
                            ),
                            caregiver = null,
                            RegistrationRequest.Input.Client(
                                null,
                                "",
                                false,
                                "Audio MIDI",
                                null,
                                userName,
                                birthday,
                                "Easy",
                                false,
                                gender,
                                null,
                                "",
                                "6"
                            )
                        )
                    )
                )
            ),
            onSuccess,
            onError
        )
    }

    private fun <T, V> initRequestSingleForResult(
        request: Single<T>,
        onSuccess: (response: V) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<T> {
        val callback =
            CallbackWrapper<T, V>(retrofit, onSuccess, onError)
        val requestSingle = request.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess {
                callback.onSuccess(it)
            }
            .doOnError {
                callback.onError(it)
            }
        callback.request = requestSingle
        return requestSingle
    }
}