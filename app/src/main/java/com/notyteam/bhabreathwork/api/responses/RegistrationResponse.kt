package com.notyteam.bhabreathwork.api.responses

import com.google.gson.annotations.SerializedName


data class RegistrationResponse(
    @field:SerializedName("register")
    val register: Register?
) {
    data class Register(
        @field:SerializedName("address")
        val address: String?,
        @field:SerializedName("caregiver")
        val caregiver: Any?,
        @field:SerializedName("client")
        val client: Client?,
        @field:SerializedName("createdAt")
        val createdAt: String?,
        @field:SerializedName("email")
        val email: String?,
        @field:SerializedName("enabledProcedures")
        val enabledProcedures: List<EnabledProcedure?>?,
        @field:SerializedName("firstName")
        val firstName: String?,
        @field:SerializedName("id")
        val id: String?,
        @field:SerializedName("isPasswordExpired")
        val isPasswordExpired: Boolean?,
        @field:SerializedName("language")
        val language: String?,
        @field:SerializedName("lastAccessed")
        val lastAccessed: Any?,
        @field:SerializedName("lastName")
        val lastName: String?,
        @field:SerializedName("lastResetPassword")
        val lastResetPassword: Any?,
        @field:SerializedName("measuringSystem")
        val measuringSystem: String?,
        @field:SerializedName("namePrefix")
        val namePrefix: String?,
        @field:SerializedName("nameSuffix")
        val nameSuffix: String?,
        @field:SerializedName("phone")
        val phone: String?,
        @field:SerializedName("roles")
        val roles: List<String?>?,
        @field:SerializedName("status")
        val status: String?,
        @field:SerializedName("userType")
        val userType: String?,
        @field:SerializedName("username")
        val username: String?
    ) {
        data class Client(
            @field:SerializedName("audioFile")
            val audioFile: String?,
            @field:SerializedName("audioGuide")
            val audioGuide: Boolean?,
            @field:SerializedName("audioType")
            val audioType: String?,
            @field:SerializedName("caregiverRegistrationNumber")
            val caregiverRegistrationNumber: String?,
            @field:SerializedName("dateOfBirth")
            val dateOfBirth: String?,
            @field:SerializedName("difficulty")
            val difficulty: String?,
            @field:SerializedName("disclaimerAccepted")
            val disclaimerAccepted: Boolean?,
            @field:SerializedName("gender")
            val gender: String?,
            @field:SerializedName("guide")
            val guide: Any?,
            @field:SerializedName("heathNotes")
            val heathNotes: String?,
            @field:SerializedName("id")
            val id: String?,
            @field:SerializedName("lastTested")
            val lastTested: Any?,
            @field:SerializedName("preferredBreathRate")
            val preferredBreathRate: String?
        )

        data class EnabledProcedure(
            @field:SerializedName("createdAt")
            val createdAt: Any?,
            @field:SerializedName("id")
            val id: String?,
            @field:SerializedName("isEnabled")
            val isEnabled: Boolean?,
            @field:SerializedName("name")
            val name: String?,
            @field:SerializedName("procedureId")
            val procedureId: String?
        )
    }
}