package com.notyteam.bhabreathwork.api

import com.google.gson.annotations.SerializedName

data class ApiError(
        @field:SerializedName("message")
        val message: String? = null,
        @field:SerializedName("path")
        val path: List<String>? = null
)