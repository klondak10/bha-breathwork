package com.notyteam.bhabreathwork.ui.workspace

import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.notyteam.bhabreathwork.MainViewModel
import com.notyteam.bhabreathwork.R
import com.notyteam.bhabreathwork.base.BaseFragment
import com.notyteam.bhabreathwork.utils.injectViewModel

class WorkspaceFragment : BaseFragment<MainViewModel>() {
    override fun layout() = R.layout.fragment_workspace

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
    }

    private fun initToolbar() {
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setupCenterText(getString(R.string.app_name))
        baseActivity.getActionBarView()?.showCenterText(true)
        baseActivity.getActionBarView()?.showLeftButton(false)
    }
}