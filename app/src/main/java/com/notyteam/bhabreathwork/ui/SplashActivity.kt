package com.notyteam.bhabreathwork.ui

import androidx.lifecycle.ViewModelProvider
import com.notyteam.bhabreathwork.R
import com.notyteam.bhabreathwork.base.BaseActivity
import com.notyteam.bhabreathwork.base.base_action_bar.ActionBarContract
import com.notyteam.bhabreathwork.base.base_action_bar.BaseActionBarPresenter
import com.notyteam.bhabreathwork.base.base_action_bar.BaseActionBarView
import com.notyteam.bhabreathwork.ui.auth.AuthFragment
import com.notyteam.bhabreathwork.MainViewModel
import com.notyteam.bhabreathwork.utils.injectViewModel
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity<MainViewModel>() {
    private lateinit var actionBarView: ActionBarContract.View
    private lateinit var actionBarPresenter: ActionBarContract.Presenter

    override fun layout() = R.layout.activity_splash

    override fun initialization() {
        fragmentManager.replaceFragment(AuthFragment())

        actionBarView = BaseActionBarView(toolbar, activity_login)
        actionBarPresenter = BaseActionBarPresenter(actionBarView)
    }

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun getActionBarView(): ActionBarContract.View? = actionBarView
}