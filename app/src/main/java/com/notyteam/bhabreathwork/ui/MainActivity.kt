package com.notyteam.bhabreathwork.ui

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import com.notyteam.bhabreathwork.MainViewModel
import com.notyteam.bhabreathwork.R
import com.notyteam.bhabreathwork.base.BaseActivity
import com.notyteam.bhabreathwork.base.base_action_bar.ActionBarContract
import com.notyteam.bhabreathwork.base.base_action_bar.BaseActionBarPresenter
import com.notyteam.bhabreathwork.base.base_action_bar.BaseActionBarView
import com.notyteam.bhabreathwork.ui.workspace.WorkspaceFragment
import com.notyteam.bhabreathwork.utils.injectViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainViewModel>() {
    private lateinit var actionBarView: ActionBarContract.View
    private lateinit var actionBarPresenter: ActionBarContract.Presenter

    companion object {
        fun start(baseActivity: Activity) {
            baseActivity.startActivity(
                Intent(baseActivity, MainActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
    }

    override fun layout() = R.layout.activity_main

    override fun initialization() {
        fragmentManager.replaceFragment(WorkspaceFragment())

        actionBarView = BaseActionBarView(toolbar, activity_login)
        actionBarPresenter = BaseActionBarPresenter(actionBarView)
    }

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun getActionBarView(): ActionBarContract.View? = actionBarView
}