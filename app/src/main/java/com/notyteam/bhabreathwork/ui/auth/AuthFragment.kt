package com.notyteam.bhabreathwork.ui.auth

import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.core.text.set
import androidx.core.text.toSpannable
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputLayout
import com.notyteam.bhabreathwork.MainViewModel
import com.notyteam.bhabreathwork.R
import com.notyteam.bhabreathwork.api.responses.AuthResponse
import com.notyteam.bhabreathwork.api.responses.FullAuthResponse
import com.notyteam.bhabreathwork.base.BaseFragment
import com.notyteam.bhabreathwork.ui.MainActivity
import com.notyteam.bhabreathwork.ui.registration.RegistrationFragment
import com.notyteam.bhabreathwork.utils.SimpleTextWatcher
import com.notyteam.bhabreathwork.utils.injectViewModel
import kotlinx.android.synthetic.main.fragment_auth.*

class AuthFragment : BaseFragment<MainViewModel>() {
    override fun layout() = R.layout.fragment_auth

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        btnSubmit.setOnClickListener {
            if (checkValidFields()) {
                showLoading()
                authorize()
            }
        }

        setRefreshErrorListener(tilLogin)
        setRefreshErrorListener(tilPassword)

        val registrationText = ("Don't have an account yet? Register").toSpannable()
        registrationText[27..35] = object : ClickableSpan() {
            override fun onClick(widget: View) {
                fragmentManager.addFragment(RegistrationFragment())
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }

        tvRegister.movementMethod = LinkMovementMethod()
        tvRegister.text = registrationText
    }

    private fun setRefreshErrorListener(til: TextInputLayout) {
        til.editText!!.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    til.error = null
                    til.isErrorEnabled = false
                }
            }
        })
    }

    private fun authorize() {
        viewModel.auth(etPassword.text.toString(), etLogin.text.toString())
            .observe(this) {
                hideLoading()

                if (it is FullAuthResponse) {
                    showAuthSuccess(it)
                } else if (it is String) {
                    showError(it)
                }
            }
    }

    private fun showAuthSuccess(authResponse: FullAuthResponse) {
        // TODO: 07.09.2020 сохранить полученные данные в piper и проверить масси процедур
        this.activity?.let {
            MainActivity.start(it)
        }

        activity?.finish()
    }

    private fun showError(errorMessage: String) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun checkValidFields(): Boolean {
        var errorCount = 0

        if (etLogin.text.toString().isEmpty()) {
            errorCount += 1
            tilLogin.isErrorEnabled = true
            tilLogin.error = getString(R.string.required_field)
        }

        if (etPassword.text.toString().isEmpty()) {
            errorCount += 1
            tilPassword.isErrorEnabled = true
            tilPassword.error = getString(R.string.required_field)
        }

        return (errorCount == 0)
    }

    override fun onBackPressed(): Boolean {
        baseActivity.finish()
        return super.onBackPressed()
    }

    private fun showLoading() {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        progressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressBar.visibility = View.GONE
    }

    private fun initToolbar() {
        baseActivity.getActionBarView()?.showActionBar(false)
        baseActivity.getActionBarView()?.showCenterText(false)
        baseActivity.getActionBarView()?.showLeftButton(false)
    }
}