package com.notyteam.bhabreathwork.ui.registration

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputLayout
import com.notyteam.bhabreathwork.MainViewModel
import com.notyteam.bhabreathwork.R
import com.notyteam.bhabreathwork.api.responses.RegistrationResponse
import com.notyteam.bhabreathwork.base.BaseFragment
import com.notyteam.bhabreathwork.utils.SimpleTextWatcher
import com.notyteam.bhabreathwork.utils.StringUtils
import com.notyteam.bhabreathwork.utils.injectViewModel
import kotlinx.android.synthetic.main.fragment_registration.*
import java.util.*

class RegistrationFragment : BaseFragment<MainViewModel>() {
    override fun layout() = R.layout.fragment_registration

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        cbMale.setOnCheckedChangeListener { _, isChecked -> cbFemale.isChecked = !isChecked }
        cbFemale.setOnCheckedChangeListener { _, isChecked -> cbMale.isChecked = !isChecked }

        setRefreshErrorListener(tilFirstName)
        setRefreshErrorListener(tilLastName)
        setRefreshErrorListener(tilEmail)
        setRefreshErrorListener(tilUserName)
        setRefreshErrorListener(tilPassword)
        setRefreshErrorListener(tilRepeatPassword)

        btnSubmit.setOnClickListener { registerNewUser() }

        etBirthday.setOnClickListener {
            val year: Int
            val month: Int
            val day: Int

            if (etBirthday.text?.isEmpty()!!) {
                val c = Calendar.getInstance()
                year = c.get(Calendar.YEAR)
                month = c.get(Calendar.MONTH)
                day = c.get(Calendar.DAY_OF_MONTH)
            } else {
                val dates: List<String> = etBirthday.text.toString().split("/")
                day = dates[0].toInt()
                month = dates[1].toInt()
                year = dates[2].toInt()
            }

            val datePickerDialog = activity?.let { it ->
                DatePickerDialog(
                    it,
                    AlertDialog.THEME_HOLO_DARK,
                    { _, year, monthOfYear, dayOfMonth ->
                        etBirthday.setText("$dayOfMonth/$monthOfYear/$year")
                    },
                    year,
                    month,
                    day
                )
            }

            datePickerDialog?.show()
        }
    }

    private fun setRefreshErrorListener(til: TextInputLayout) {
        til.editText!!.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    til.error = null
                    til.isErrorEnabled = false
                }
            }
        })
    }

    private fun registerNewUser() {
        if (checkValidFields()) {
            showLoading()

            val gender = if (cbMale.isChecked) "Male" else "Female"

            viewModel.registrationUser(
                etFirstName.text.toString(),
                etLastName.text.toString(),
                gender,
                etBirthday.text.toString(),
                etEmail.text.toString(),
                etUserName.text.toString(),
                etPassword.text.toString()
            ).observe(this, {
                hideLoading()
                if (it is RegistrationResponse) {
                    showRegistrationSuccess(it)
                } else if (it is String) {
                    showError(it)
                }
            })
        }
    }

    private fun showRegistrationSuccess(registrationResponse: RegistrationResponse) {
        Toast.makeText(
            context,
            getString(R.string.registration_success),
            Toast.LENGTH_LONG
        ).show()

        onBackPressed()
    }

    private fun showError(errorMessage: String) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
        onBackPressed()
    }

    private fun checkValidFields(): Boolean {
        var errorCount = 0

        if (etFirstName.text.toString().isEmpty()) {
            errorCount += 1
            tilFirstName.isErrorEnabled = true
            tilFirstName.error = getString(R.string.required_field)
        }

        if (etLastName.text.toString().isEmpty()) {
            errorCount += 1
            tilLastName.isErrorEnabled = true
            tilLastName.error = getString(R.string.required_field)
        }

        if (!StringUtils.isValidEmail(etEmail.text.toString())) {
            errorCount += 1
            tilEmail.isErrorEnabled = true
            tilEmail.error = getString(R.string.incorrect_email)
        }

        if (etUserName.text.toString().isEmpty()) {
            errorCount += 1
            tilUserName.isErrorEnabled = true
            tilUserName.error = getString(R.string.required_field)
        }

        if (etPassword.text.toString().length < 4) {
            errorCount += 1
            tilPassword.isErrorEnabled = true
            tilPassword.error = getString(R.string.cannot_be_less)
        }

        if (etRepeatPassword.text.toString().length < 4) {
            errorCount += 1
            tilRepeatPassword.isErrorEnabled = true
            tilRepeatPassword.error = getString(R.string.cannot_be_less)
        }

        if (etPassword.text.toString() != etRepeatPassword.text.toString()) {
            errorCount += 1
            tilRepeatPassword.isErrorEnabled = true
            tilRepeatPassword.error = getString(R.string.password_mismatch)
        }

        return (errorCount == 0)
    }

    private fun showLoading() {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        progressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressBar.visibility = View.GONE
    }

    private fun initToolbar() {
        baseActivity.getActionBarView()?.showActionBar(false)
        baseActivity.getActionBarView()?.showCenterText(false)
        baseActivity.getActionBarView()?.showLeftButton(false)
    }
}