package com.notyteam.bhabreathwork.utils.paper

import io.paperdb.Paper

class PaperIO {
    companion object {
        private const val ACCESS_TOKEN = "ACCESS_TOKEN"

        fun setAccessToken(token: String) {
            Paper.book().write(ACCESS_TOKEN, token)
        }

        fun getAccessToken(): String {
            return try {
                Paper.book().read(ACCESS_TOKEN) ?: ""
            } catch (e: Exception) {
                ""
            }
        }

        fun clear() {
            Paper.book().allKeys.forEach {
                Paper.book().delete(it)
            }
        }
    }
}