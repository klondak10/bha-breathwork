package com.notyteam.bhabreathwork.utils.ciceron

interface OnBackClickListener {
    fun onBackPressed(): Boolean
}