package com.notyteam.bhabreathwork.utils.ciceron

import com.notyteam.bhabreathwork.base.BaseFragment
import com.notyteam.bhabreathwork.base.mvvm.BaseViewModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    class FragmentScreen<P : BaseViewModel, T : BaseFragment<P>>(var fragment: T) :
        SupportAppScreen() {
        override fun getFragment(): androidx.fragment.app.Fragment {
            return fragment
        }
    }
}