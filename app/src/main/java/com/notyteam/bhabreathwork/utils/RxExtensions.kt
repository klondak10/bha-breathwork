package com.notyteam.bhabreathwork.utils

import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import com.notyteam.bhabreathwork.R

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Button.enable() {
    this.isEnabled = true
}

fun Button.disable() {
    this.isEnabled = false
}

fun showToast(toast: Toast, message: String) {
    if (toast.view.windowVisibility != View.VISIBLE) {
        toast.setText(message)
        toast.show()
    }
}

fun View.shake() {
    this.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.shake))
}

fun View.scrollToView(layout: ViewGroup?) {
    if (layout is NestedScrollView) {
        val rectf = Rect()
        this.getLocalVisibleRect(rectf)
//    this.getGlobalVisibleRect(rectf)
        val y =
            if (rectf.top > 0) -rectf.top else rectf.top// - convertDpToPixel(TOOLBAR_HEIGHT_DP, scrollView.context)
        layout.scrollBy(0, y)
    }
}

fun EditText.setError(isError: Boolean) {
    this.setBackgroundResource(
        if (isError) R.drawable.ic_back_arrow
        else R.drawable.ic_back_arrow
    )
}

fun EditText.validate(
    validationCriteria: Boolean,
    viewForShake: View,
    viewForScrollingTo: View? = null,
    container: ViewGroup? = null
): Boolean {
    if (!validationCriteria) {
        viewForScrollingTo?.scrollToView(container)
        viewForShake.shake()
    }
    this.setError(!validationCriteria)
    return validationCriteria
}

sealed class Result<T>
data class Success<T>(val value: T) : Result<T>()
data class Failure<T>(val errorMessage: String, val errorCode: Int) : Result<T>()
data class Loading<T>(val isLoading: Boolean) : Result<T>()

//infix fun <T> Result<T>.then(f: (T) -> Result<T>) =
//        when (this) {
//            is Success -> f(this.value)
//            is Failure -> this
//            is Loading -> Loading(false)
//        }
//
//infix fun <T> Result<T>.otherwise(f: (String, Int) -> Unit) =
//        if (this is Failure) f(this.errorMessage, this.errorCode) else Unit

//fun chlen2(s: String): Result<String> {
//    return Success(s)
//}
//
//fun error(s: String, code: Int) {
//
//}
//
//fun ddd(){
//    Failure<String>("chlen", 0) then ::chlen2 otherwise ::error
//}